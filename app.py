import connexion
from connexion import NoContent
import datetime
import json
import logging
import logging.config
import pykafka
from pykafka import KafkaClient
import requests
import uuid
from sqlalchemy.engine import url
import yaml 

headers = { 'Content-Type': 'application/json' }
with open('app_conf.yml', 'r') as f:
    app_config = yaml.safe_load(f.read())

def process_event(event, endpoint):
    trace_id = str(uuid.uuid4())
    event['trace_id'] = trace_id

    logger.debug(f'Received {endpoint} event with trace id {trace_id}')

    client = KafkaClient(hosts=app_config['events']['hostname']+':'+str(app_config['events']['port']))

    topic = client.topics[app_config['events']['topic']]

    producer = topic.get_sync_producer()

    event_to_send = {
    'type': endpoint,
    'datetime': datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
    'payload': event
}
    
    json_event = json.dumps(event_to_send)

    producer.produce(json_event.encode('utf-8'))

    logger.debug(f'PRODUCER::producing {endpoint} event')

    logger.debug(json_event)

    return NoContent, 201


def buy(body):
    process_event(body, 'buy')
    return NoContent, 201

def sell(body):
    process_event(body, 'sell')
    return NoContent, 201

app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api("openapi.yml", strict_validation=True, validate_responses=True)


with open('log_conf.yml', 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basic')
logging.info('Assignment 3.1')

if __name__ == "__main__":
    app.run(port=8080)
